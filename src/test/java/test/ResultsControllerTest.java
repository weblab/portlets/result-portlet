/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.Constants;
import org.ow2.weblab.portlet.results.bean.NavigationBean;
import org.ow2.weblab.portlet.results.controllers.ResultsController;
import org.ow2.weblab.portlet.results.services.ResultsServiceImpl;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.mock.web.portlet.MockEvent;
import org.springframework.mock.web.portlet.MockEventRequest;
import org.springframework.mock.web.portlet.MockEventResponse;

public class ResultsControllerTest {
	ResultsController controller;
	MockEventRequest request;
	MockEventResponse response;
	NavigationBean navigationBean;
	ResultSet resultSet;

	@Before
	public void initEachTest() throws WebLabCheckedException {
		WebLabMarshaller webLabMarshaller = new WebLabMarshaller();
		this.resultSet = webLabMarshaller.unmarshal(new File("src/test/resources/resultSet.xml"), ResultSet.class);
		MockEvent event = new MockEvent(Constants.REACTION_DISPLAY_RESULTS, this.resultSet);
		this.request = new MockEventRequest(event);
		// request.setRemoteUser(USER_ID);
		this.response = new MockEventResponse();
		this.controller = new ResultsController();
		this.navigationBean = new NavigationBean();
		this.controller.setResultsService(new ResultsServiceImpl());

	}

	@Test
	public void testDisplayResults() throws Exception {
		this.controller.displayResults(this.request, this.response, this.navigationBean);
		assertEquals(2, this.navigationBean.getPagination().getTotalItems());
		assertEquals(2, this.navigationBean.getHits().size());
		assertEquals("Evaluation With the VIRTUOSO Platform", this.navigationBean.getHits().get(1).getResource().getTitle());
		assertEquals("An open source platform for information extraction and retrieval evaluation.", this.navigationBean.getHits().get(1).getResource().getSubject());
		assertEquals("en", this.navigationBean.getHits().get(1).getResource().getLanguage());
		assertEquals("application/pdf", this.navigationBean.getHits().get(1).getResource().getFormat());
	}

	@Test
	public void testLoadHugeRS() throws Exception {
		DublinCoreAnnotator dublinCoreAnnotator = new DublinCoreAnnotator(this.resultSet.getPok());
		dublinCoreAnnotator.startInnerAnnotatorOn(new URI("weblab://indexsearch.solr/hit14"));
		for (int i = 0; i < 10000; i++) {
			dublinCoreAnnotator.writeType("type" + i);

		}
		this.controller.displayResults(this.request, this.response, this.navigationBean);
		assertEquals(2, this.navigationBean.getPagination().getTotalItems());
		assertEquals(2, this.navigationBean.getHits().size());
		assertEquals("Evaluation With the VIRTUOSO Platform", this.navigationBean.getHits().get(1).getResource().getTitle());
	}
}
