<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>

<%@page import="org.ow2.weblab.portlet.results.Constants"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>


<portlet:resourceURL var="displayResultsURL" id="<%= Constants.DISPLAY_RESULTS_RESOURCE %>"
 	escapeXml="false">
 	<portlet:param name="utf8" value="✓" />
 </portlet:resourceURL>

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<div id="result_portlet_content" class='contenu_portlet'>

<!-- is there an error message -->
		<c:if test="${not empty errorMsg}">
			<div class="portlet-msg-error">${errorMsg }</div>
		</c:if>

<c:choose>
	<c:when test="${navigationBean.pagination.totalItems==null || navigationBean.pagination.totalItems==0}">
		<div class="portlet-msg-info">
			<fmt:message key="portlet.no_results" />
		</div>
	</c:when>
	<c:otherwise>

		<!-- icons definition -->
		<c:url value="/images/" var="baseURL"></c:url>
		
		<div id="result_portlet_top_nbresults" class="portlet-msg-info">
			<!-- number of results -->
			${navigationBean.pagination.totalItems} <fmt:message key="portlet.results.nb" />
		</div>
		<div>
			<!--  result_portlet_view_resourceCollection.jsp to display results list -->
			<jsp:include page="result_portlet_view_resourceCollection.jsp" flush="true" />
		</div>		
	</c:otherwise>
</c:choose>

</div>



