<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="org.ow2.weblab.portlet.results.bean.NavigationBean"%>
<%@page import="org.ow2.weblab.portlet.results.Constants"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<portlet:defineObjects />
<!--rendering resourceCollection  -->


<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="result_portlet" />


<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<table class="taglib-search-iterator" id="resultPortlet_table">
	<thead>

		<!-- header -->
		<tr class="portlet-section-header results-header basket_row-title">
			<th>
			<c:if test="${navigationBean.displayPagination}">
				<!-- Navigation div -->
 				<div id="result_portlet_top_result_navigation">
 				<!--  first page is not in page listing : we add it "manually" --> 
				<c:if test="${navigationBean.farFromStartPage}">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="1" />
					</portlet:actionURL>
	 				<a href="${updatePageURL}">1</a>
				</c:if>
				<c:if test="${navigationBean.veryFarFromStartPage}">...</c:if>
				
				<c:forEach items="${navigationBean.curPages}" var="p" varStatus="status">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="${p }" />
					</portlet:actionURL>
	 				<c:choose>
	 					<c:when test="${p==navigationBean.curPage}">
	 						<span class="split_current_position" >${p}</span>
	 					</c:when>
	 					<c:otherwise>
	 						<a  href="${updatePageURL}">${p}</a>
	 					</c:otherwise>
	 				</c:choose>
	 			</c:forEach>
	 			<c:if test="${navigationBean.veryFarFromEndPage }">...</c:if>
				<c:if test="${navigationBean.farFromEndPage }">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="${navigationBean.lastPage}" />
					</portlet:actionURL>
	 				<a href="${updatePageURL}">${navigationBean.lastPage}</a>
				</c:if>
 				</div>
 			</c:if>
			</th>
		</tr>

	</thead>
	<tbody>
		
	<!-- iteration on resource collection -->		
	<c:forEach var="hit" items="${navigationBean.hits}" varStatus="status">
		
		<!-- hit line-->
		<tr
		<c:choose>
			<c:when test="${(status.count%2) == 1}">
				class="portlet-section-body results-row" onmouseover="jQuery.hit_mouseover(this);" onclick="jQuery.hit_click(this);" onmouseout="jQuery.hit_mouseout(this);"
			</c:when>
			<c:otherwise>
				class="portlet-section-alternate results-row alt" onmouseover="jQuery.hit_mouseover_alternate(this);" onclick="jQuery.hit_click_alternate(this);" onmouseout="jQuery.hit_mouseout_alternate(this);"
			</c:otherwise>
		</c:choose>
		>
			<td>
			
			<c:if test="${!empty hit.resource.thumbnail}">
				
				<!-- Thumbnail div-->
				<div class="thumbnail">
					<img class="thumbnail_image" alt="<fmt:message key="thumbnail.image.alt"/>" src="${hit.resource.thumbnail}"/>
				</div>
			</c:if>
			
			<c:if test="${!empty hit.resource.hasThumbnailSnippet}">
				<c:choose>
					<c:when test="${fn:length(hit.resource.hasThumbnailSnippet) > 1}">
						<!-- Multiple thumbnails snippet div-->	
						<div class="thumbnail_multiple">
							<c:forEach var="thumb" items="${hit.resource.hasThumbnailSnippet}">
								<img class="thumbnail_image" alt="<fmt:message key="thumbnail.image.alt"/>" src="${thumb}"/>
							</c:forEach>
						</div>
					
					</c:when>
					<c:otherwise>
						<div class="thumbnail">
							<img class="thumbnail_image" alt="<fmt:message key="thumbnail.image.alt"/>" src="hit.resource.hasThumbnailSnippet[0]"/>
						</div>	
					</c:otherwise>
				</c:choose>
			</c:if>
			
				<div 
				<c:choose>
					<c:when test="${!empty hit.resource.hasThumbnailSnippet}">
						class="hitDesc textAndthumb"
					</c:when>
					<c:otherwise>
						class="hitDesc"
					</c:otherwise>
				</c:choose>
				>
				
				<!-- hit first line -->
				
						<h5>
							
							<!-- icon -->
							<c:if test="${!empty hit.resource.isExposedAs}">
							<a class="format_link"  target="_blank"
								href="${hit.resource.isExposedAs}">
							</c:if>
							<c:choose>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'html')}">
									<img src="${baseURL}html.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'rtf')}">
									<img src="${baseURL}rtf.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'xml')}">
									<img src="${baseURL}xml.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'rss')}">
									<img src="${baseURL}rss-icon.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'pdf')}">
									<img src="${baseURL}pdf.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'word')}">
									<img src="${baseURL}doc.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'powerpoint')}">
									<img src="${baseURL}ppt.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'excel')}">
									<img src="${baseURL}xls.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'visio')}">
									<img src="${baseURL}vsd.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'opendocument.spreadsheet')}">
									<img src="${baseURL}ods.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'opendocument.text')}">
									<img src="${baseURL}odt.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'opendocument.presentation')}">
									<img src="${baseURL}odp.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'opendocument.graphics')}">
									<img src="${baseURL}odg.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'gif')}">
									<img src="${baseURL}gif.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'jpeg')}">
									<img src="${baseURL}jpg.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'png')}">
									<img src="${baseURL}png.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'bitmap')}">
									<img src="${baseURL}bmp.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'bmp')}">
									<img src="${baseURL}bmp.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'zip')}">
									<img src="${baseURL}zip.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'wav')}">
									<img src="${baseURL}sound.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'audio.mpeg')}">
									<img src="${baseURL}sound.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'video.mpeg')}">
									<img src="${baseURL}video.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'quicktime')}">
									<img src="${baseURL}quicktime.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'msvideo')}">
									<img src="${baseURL}wmv.png" />
								</c:when>
								<c:when
									test="${fn:containsIgnoreCase(hit.resource.format,'movie')}">
									<img src="${baseURL}video.png" />
								</c:when>
								<c:otherwise> - </c:otherwise>
							
							</c:choose>
							<c:if test="${!empty hit.resource.isExposedAs}">
							</a>
							</c:if>
							
							<!-- language icon -->					
							<c:choose>
								<c:when test="${!empty hit.resource.language}">
									<img src="${baseURL}/flags/${hit.resource.language}.png" />
								</c:when>
							 	<c:otherwise> - </c:otherwise>
							</c:choose>
							<!-- action url for title-->
							 
							<c:choose>
								<c:when test="${!empty hit.resource.hasRelevantMediaUnit}">
									<portlet:actionURL name="<%= Constants.DOCUMENT_SELECTION_ACTION %>" var="load_resource_url" escapeXml="false">
										<portlet:param name="utf8" value="✓" />
										<portlet:param name="<%= Constants.DOCUMENT_URI_PARAM %>" value="${hit.resource.URI}@relevantMu:${hit.resource.hasRelevantMediaUnit}" />
									</portlet:actionURL>								
								</c:when>
								<c:otherwise>
									<portlet:actionURL name="<%= Constants.DOCUMENT_SELECTION_ACTION %>" var="load_resource_url" escapeXml="false">
										<portlet:param name="utf8" value="✓" />
										<portlet:param name="<%= Constants.DOCUMENT_URI_PARAM %>" value="${hit.resource.URI}" />
									</portlet:actionURL>
								</c:otherwise>
							</c:choose>
								
							<!-- linked title -->
							<a href="${load_resource_url}">
                            (<fmt:formatDate value="${hit.resource.gatheringDate}" pattern="yyyy-MM-dd"/>) -
							<c:choose>
								<c:when test="${empty hit.resource.hasOriginalFileName && empty hit.resource.title && empty hit.resource.firstName && empty hit.resource.lastName}">
									<fmt:message key="meta.defaultTitle" />	
								</c:when>
								
								<c:otherwise>
									<c:choose>
										<c:when test="${!empty hit.resource.title}">
											${hit.resource.title}
										</c:when>
										<c:when test="${!empty hit.resource.firstName}">
											${ hit.resource.firstName}&nbsp;${hit.resource.lastName}
										</c:when>
										<c:otherwise>
											${hit.resource.hasOriginalFileName}
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							</a>						
						</h5>
										
					<!-- hit text Preview -->
					 
					<div class="result_portlet_text_preview">
						${hit.hasDescription}
					</div>
					
					<!-- meta-data -->
					
					<div class="RESULT_META_CLASS">
						<img class="result_portlet-slidable_entry-expander feed-entry-expander" src="${baseURL}01_plus.png">
						<span><fmt:message key="meta.metadata"/></span>
						
						<div class="result_portlet-feed-entry-content">
							<table class="result_portlet_metadata_table">
							
							<c:if test="${meta_conf_bean.properties['hasScore'] && ! empty hit.hasScore}">
								<tr>
									<td><u><fmt:message key="meta.hasScore" />:</u></td>
									<td><i>${hit.hasScore}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['hasNativeContent'] && ! empty hit.resource.hasNativeContent}">
								<tr>
									<td><u><fmt:message key="meta.hasNativeContent" />:</u></td>
									<td><i>${hit.resource.hasNativeContent}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['hasNormalisedContent'] && ! empty hit.resource.hasNormalisedContent}">
								<tr>
									<td><u><fmt:message key="meta.hasNormalisedContent" />:</u></td>
									<td><i>${hit.resource.hasNormalisedContent}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['isExposedAs'] && ! empty hit.resource.isExposedAs}">
								<tr>
									<td><u><fmt:message key="meta.isExposedAs" />:</u></td>
									<td><i>
											<a target="_blank" href="${hit.resource.isExposedAs}">${hit.resource.isExposedAs}</a>
										</i>
									</td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['isGeneratedFrom'] && ! empty hit.resource.isGeneratedFrom}">
								<tr>
									<td><u><fmt:message key="meta.isGeneratedFrom" />:</u></td>
									<td><i>${hit.resource.isGeneratedFrom}</i></td>
								</tr>
							</c:if>
<%-- 							<c:if test="${meta_conf_bean.properties['isResultOf'] && ! empty hit.isResultOf}"> --%>
<!-- 								<tr> -->
<%-- 									<td><u><fmt:message key="meta.isResultOf" />:</u></td> --%>
<%-- 									<td><i>${hit.isResultOf}</i></td> --%>
<!-- 								</tr> -->
<%-- 							</c:if> --%>
							<c:if test="${meta_conf_bean.properties['isProducedBy'] && ! empty hit.resource.isProducedBy}">
								<tr>
									<td><u><fmt:message key="meta.isProducedBy" />:</u></td>
									<td><i>${hit.resource.isProducedBy}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['creator'] && ! empty hit.resource.creator}">
								<tr>
									<td><u><fmt:message key="meta.creator" />:</u></td>
									<td><i>${hit.resource.creator}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['date'] && ! empty hit.resource.date}">
								<tr>
									<td><u><fmt:message key="meta.date" />:</u></td>
									<td><i>${hit.resource.date}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['description'] && ! empty hit.resource.description}">
								<tr>
									<td><u><fmt:message key="meta.description" />:</u></td>
									<td><i>${hit.resource.description}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['format'] && ! empty hit.resource.format}">
								<tr>
									<td><u><fmt:message key="meta.format" />:</u></td>
									<td><i>${hit.resource.format}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['identifier'] && ! empty hit.resource.identifier}">
								<tr>
									<td><u><fmt:message key="meta.identifier" />:</u></td>
									<td><i>${hit.resource.identifier}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['language'] && ! empty hit.resource.language}">
								<tr>
									<td><u><fmt:message key="meta.language" />:</u></td>
									<td><i>${hit.resource.language}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['source'] && ! empty hit.resource.source}">
								<tr>
									<td><u><fmt:message key="meta.source" />:</u></td>
									<td><i>${hit.resource.source}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['subject'] && ! empty hit.resource.subject}">
								<tr>
									<td><u><fmt:message key="meta.subject" />:</u></td>
									<td><i>${hit.resource.subject}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['title'] && ! empty hit.resource.title}">
								<tr>
									<td><u><fmt:message key="meta.title" />:</u></td>
									<td><i>${hit.resource.title}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['type'] && ! empty hit.resource.type}">
								<tr>
									<td><u><fmt:message key="meta.type" />:</u></td>
									<td><i>${hit.resource.type}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['coverage'] && ! empty hit.resource.coverage}">
								<tr>
									<td><u><fmt:message key="meta.coverage" />:</u></td>
									<td><i>${hit.resource.coverage}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['extent'] && ! empty hit.resource.extent}">
								<tr>
									<td><u><fmt:message key="meta.extent" />:</u></td>
									<td><i>${hit.resource.extent}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['created'] && ! empty hit.resource.created}">
								<tr>
									<td><u><fmt:message key="meta.created" />:</u></td>
									<td><i>${hit.resource.created}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['gatheringDate'] && ! empty hit.resource.gatheringDate}">
								<tr>
									<td><u><fmt:message key="meta.gatheringDate" />:</u></td>
									<td><i>${hit.resource.gatheringDate}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['projectName'] && ! empty hit.resource.projectName}">
								<tr>
									<td><u><fmt:message key="meta.projectName" />:</u></td>
									<td><i>${hit.resource.projectName}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['hasOriginalFileSize'] && ! empty hit.resource.hasOriginalFileSize}">
								<tr>
									<td><u><fmt:message key="meta.hasOriginalFileSize" />:</u></td>
									<td><i>${hit.resource.hasOriginalFileSize}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['refersTo'] && ! empty hit.resource.refersTo}">
								<tr>
									<td><u><fmt:message key="meta.refersTo" />:</u></td>
									<td><i>${hit.resource.refersTo}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['catego'] && ! empty hit.resource.catego}">
								<tr>
									<td><u><fmt:message key="meta.catego" />:</u></td>
									<td><i>${hit.resource.catego}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['abstract'] && ! empty hit.resource.isAbstract}">
								<tr>
									<td><u><fmt:message key="meta.abstract" />:</u></td>
									<td><i>${hit.resource.abstract}</i></td>
								</tr>
							</c:if>
							<c:if test="${meta_conf_bean.properties['seeAlso'] && ! empty hit.resource.seeAlso}">
								<tr>
									<td><u><fmt:message key="meta.seeAlso" />:</u></td>
									<td><i>${hit.resource.seeAlso}</i></td>
								</tr>
							</c:if>
							</table>
						</div>
					</div>
				</div>
			</td>
		</tr>
		
	</c:forEach>
	</tbody>
	
	<tfoot>
	<!-- footer -->
		<tr class="portlet-section-header results-header basket_row-title">
			<th>
			<c:if test="${navigationBean.displayPagination}">
				<!-- Navigation div -->
 				<div id="result_portlet_top_result_navigation">
 				<!--  first page is not in page listing : we add it "manually" --> 
				<c:if test="${navigationBean.farFromStartPage}">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="1" />
					</portlet:actionURL>
	 				<a href="${updatePageURL}">1</a>
				</c:if>
				<c:if test="${navigationBean.veryFarFromStartPage}">...</c:if>
				
				<c:forEach items="${navigationBean.curPages}" var="p" varStatus="status">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="${p }" />
					</portlet:actionURL>
	 				<c:choose>
	 					<c:when test="${p==navigationBean.curPage}">
	 						<span class="split_current_position" >${p}</span>
	 					</c:when>
	 					<c:otherwise>
	 						<a  href="${updatePageURL}">${p}</a>
	 					</c:otherwise>
	 				</c:choose>
	 			</c:forEach>
	 			<c:if test="${navigationBean.veryFarFromEndPage }">...</c:if>
				<c:if test="${navigationBean.farFromEndPage }">
					<portlet:actionURL name="<%= Constants.UPDATE_PAGE_ACTION %>" var="updatePageURL" escapeXml="false">
						<portlet:param name="utf8" value="✓" />
						<portlet:param name="<%= Constants.PAGE_PARAM %>" value="${navigationBean.lastPage}" />
					</portlet:actionURL>
	 				<a href="${updatePageURL}">${navigationBean.lastPage}</a>
				</c:if>
 				</div>
 			</c:if>
			</th>
		</tr>
	
	</tfoot>
</table>

<script type="text/javascript">
jQuery(function() {
	 var minusImage = '01_minus.png';
	 var plusImage = '01_plus.png';
	 jQuery(".RESULT_META_CLASS").click(
	 	function() {
	 		if (this.children[0].src.indexOf('minus.png') > -1) {
	 			jQuery(".result_portlet-feed-entry-content", this).slideUp();
	 			this.children[0].src = this.children[0].src.replace(minusImage, plusImage);
	 		} else {
	 			jQuery(".result_portlet-feed-entry-content", this).slideDown();
	 			this.children[0].src = this.children[0].src.replace(plusImage, minusImage);
	 		}
	 	}
	);
	}
);
</script>
