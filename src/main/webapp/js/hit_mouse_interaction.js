jQuery.hit_mouseover = function (el) {
    if(!jQuery(el).hasClass('selectedHit')) {
       jQuery(el).attr('class', 'portlet-section-body-hover results-row hover');
    } else {
        //jQuery(el).attr('class', 'portlet-section-body-hover results-row hover selectedHit');
    }
};

jQuery.hit_mouseout = function (el) {
    if(!jQuery(el).hasClass('selectedHit')) {
        jQuery(el).attr('class', 'portlet-section-body results-row');
    } else {
    	//jQuery(el).attr('class', 'selectedHit');
    }
};

jQuery.hit_click = function (el) {
        //jQuery(el).attr('class', 'selectedHit');
};

jQuery.hit_mouseover_alternate = function (el) {
    if(!jQuery(el).hasClass('selectedHit_alternate')) {
       jQuery(el).attr('class', 'portlet-section-alternate-hover results-row alt hover');
    } else {
        //jQuery(el).attr('class', 'portlet-section-alternate-hover results-row alt hover selectedHit_alternate');
    }
};

jQuery.hit_mouseout_alternate = function (el) {
    if(!jQuery(el).hasClass('selectedHit_alternate')) {
        jQuery(el).attr('class', 'portlet-section-alternate results-row alt');   
    } else {
    	 //jQuery(el).attr('class', 'selectedHit_alternate');
    }
};

jQuery.hit_click_alternate = function (el) {
       
       // jQuery(el).attr('class', 'selectedHit_alternate');
    
};