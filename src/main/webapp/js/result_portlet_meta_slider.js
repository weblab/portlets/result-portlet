jQuery( function() {
	var minusImage = '01_minus.png';
	var plusImage = '01_plus.png';
	jQuery(".result_portlet-slidable_entry-expander").click(
			function() {
				if (this.src.indexOf('minus.png') > -1) {
					jQuery(".result_portlet-feed-entry-content",
							this.parentNode).slideUp();
					this.src = this.src.replace(minusImage, plusImage);
				} else {
					jQuery(".result_portlet-feed-entry-content",
							this.parentNode).slideDown();
					this.src = this.src = this.src.replace(plusImage,
							minusImage);
				}
			});
});