/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results;

import java.util.ArrayList;
import java.util.List;

/**
 * A small pagination implementation. Works with a total number of items, a number of items per page and give infos about pages (number of pages, current page idx has previous or next page...)
 * @author lmartin
 *
 */
public class Pagination {

	private int currentPage = 1;
	private int totalItems;
	private int itemsPerPage;

	private int totalPages = 0;

	public Pagination(int totalItems, int itemsPerPage) {

		this.totalItems = totalItems;
		this.itemsPerPage = itemsPerPage;
		if (this.itemsPerPage < 1) {
			this.itemsPerPage = 1;
		}

		this.totalPages = this.totalItems / this.itemsPerPage;
		if (this.totalItems % this.itemsPerPage > 0) {
			this.totalPages = this.totalPages + 1;
		}

	}

	public Pagination(int totalItems, int itemsPerPage,int currentItemIdx) {
		this(totalItems,itemsPerPage);
		setFirstItem(currentItemIdx);
	}

	public int getCurrentPage() {
		return this.currentPage;
	}

	public void setCurrentPage(int currentPage) {
		if (currentPage > this.totalPages) {
			currentPage = this.totalPages;
		}
		if (currentPage < 1) {
			currentPage = 1;
		}
		this.currentPage = currentPage;
	}

	public void setFirstItem(int firstItemIdx) {
		setCurrentPage(1);
		while(hasNextPage()) {
			if((getStartIndex()<=firstItemIdx)&&(firstItemIdx<=getEndIndex())) {
				break;
			}
			setCurrentPage(getCurrentPage()+1);
		}
	}

	public int getTotalPages() {
		return this.totalPages;
	}

	public boolean hasPreviousPage() {
		return this.currentPage > 1;
	}

	public boolean hasNextPage() {
		return this.currentPage < this.totalPages;
	}

	public int getPreviousPage() {
		if (hasPreviousPage()) {
			return this.currentPage - 1;
		}
		return 1;
	}

	public int getNextPage() {
		if (hasNextPage()) {
			return this.currentPage + 1;
		}
		return this.totalPages;
	}

	public int getStartIndex() {
		return (this.currentPage - 1) * this.itemsPerPage;
	}

	public int getEndIndex() {
		int endIndex = this.currentPage * this.itemsPerPage;
		if (endIndex > this.totalItems) {
			endIndex = this.totalItems;
		}
		return endIndex - 1;
	}

	/**
	 * Get index of N prev and next pages starting from current page
	 * @param radius number of pages to consider
	 * @return {@link List} "radius" number of pages before + current page + "radius" pages after
	 */
	public List<Integer> getPagesList(int radius) {
		List<Integer> pageList = new ArrayList<>();

		int startPage = getCurrentPage() - radius;
		if (startPage < 1) {
			startPage = 1;
		}

		int endPage = getCurrentPage() + radius;
		if (endPage > getTotalPages()) {
			endPage = getTotalPages();
		}

		for (int page = startPage; page <= endPage; page++) {
			pageList.add(Integer.valueOf(page));
		}

		return pageList;
	}

	public int getTotalItems() {
		return this.totalItems;
	}

	public int getItemsPerPage() {
		return this.itemsPerPage;
	}
}
