/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.bean;

import java.util.List;

import org.ow2.weblab.portlet.results.Pagination;
import org.ow2.weblab.portlet.results.rdf.beans.HitBean;

/**
 * Bean use while user navigate through results list. It will store user's preferences, current order properties, current offset / limit
 *
 * @author lmartin
 *
 */
public class NavigationBean {

	private List<HitBean> hits;

	private Pagination pagination;

	private int pagesRadius=2;

	private int curPage = 1;
	private int lastPage;
	private boolean farFromStartPage = false;
	private boolean farFromEndPage = false;
	private boolean veryFarFromStartPage = false;
	private boolean veryFarFromEndPage = false;
	private boolean displayPagination = false;
	private List<Integer> curPages;

	private String originalQueryURI;

	/**
	 * @return the hits
	 */
	public List<HitBean> getHits() {
		return this.hits;
	}

	/**
	 * @param hits the hits to set
	 */
	public void setHits(List<HitBean> hits) {
		this.hits = hits;
	}

	/**
	 * @return the pagination
	 */
	public Pagination getPagination() {
		return this.pagination;
	}

	/**
	 * @param pagination the pagination to set
	 */
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
		if (this.pagination != null) {
			this.displayPagination = true;
			this.curPage = pagination.getCurrentPage();
			this.curPages = pagination.getPagesList(this.pagesRadius);
			this.farFromStartPage = (pagination.getTotalPages()>1)&&((!this.curPages.contains(Integer.valueOf(1))));
			this.veryFarFromStartPage = (pagination.getTotalPages()>1)&&(!this.curPages.contains(Integer.valueOf(2)));
			this.farFromEndPage = (pagination.getTotalPages()>1)&&(!this.curPages.contains(Integer.valueOf(pagination.getTotalPages())));
			this.veryFarFromEndPage = (pagination.getTotalPages()>1)&&(!this.curPages.contains(Integer.valueOf(pagination.getTotalPages() - 1)));
			this.lastPage = pagination.getTotalPages();
		}
	}

	/**
	 * @return the curPage
	 */
	public int getCurPage() {
		return this.curPage;
	}

	/**
	 * @param curPage the curPage to set
	 */
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	/**
	 * @return the lastPage
	 */
	public int getLastPage() {
		return this.lastPage;
	}

	/**
	 * @param lastPage the lastPage to set
	 */
	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}

	/**
	 * @return the farFromStartPage
	 */
	public boolean isFarFromStartPage() {
		return this.farFromStartPage;
	}

	/**
	 * @param farFromStartPage the farFromStartPage to set
	 */
	public void setFarFromStartPage(boolean farFromStartPage) {
		this.farFromStartPage = farFromStartPage;
	}

	/**
	 * @return the farFromEndPage
	 */
	public boolean isFarFromEndPage() {
		return this.farFromEndPage;
	}

	/**
	 * @param farFromEndPage the farFromEndPage to set
	 */
	public void setFarFromEndPage(boolean farFromEndPage) {
		this.farFromEndPage = farFromEndPage;
	}

	/**
	 * @return the veryFarFromStartPage
	 */
	public boolean isVeryFarFromStartPage() {
		return this.veryFarFromStartPage;
	}

	/**
	 * @param veryFarFromStartPage the veryFarFromStartPage to set
	 */
	public void setVeryFarFromStartPage(boolean veryFarFromStartPage) {
		this.veryFarFromStartPage = veryFarFromStartPage;
	}

	/**
	 * @return the veryFarFromEndPage
	 */
	public boolean isVeryFarFromEndPage() {
		return this.veryFarFromEndPage;
	}

	/**
	 * @param veryFarFromEndPage the veryFarFromEndPage to set
	 */
	public void setVeryFarFromEndPage(boolean veryFarFromEndPage) {
		this.veryFarFromEndPage = veryFarFromEndPage;
	}

	/**
	 * @return the displayPagination
	 */
	public boolean isDisplayPagination() {
		return this.displayPagination;
	}

	/**
	 * @param displayPagination the displayPagination to set
	 */
	public void setDisplayPagination(boolean displayPagination) {
		this.displayPagination = displayPagination;
	}

	/**
	 * @return the curPages
	 */
	public List<Integer> getCurPages() {
		return this.curPages;
	}

	/**
	 * @param curPages the curPages to set
	 */
	public void setCurPages(List<Integer> curPages) {
		this.curPages = curPages;
	}

	/**
	 * @return the pagesRadius
	 */
	public int getPagesRadius() {
		return this.pagesRadius;
	}

	/**
	 * @param pagesRadius the pagesRadius to set
	 */
	public void setPagesRadius(int pagesRadius) {
		this.pagesRadius = pagesRadius;
	}

	/**
	 * @return the originalQueryURI
	 */
	public String getOriginalQueryURI() {
		return this.originalQueryURI;
	}

	/**
	 * @param originalQueryURI the originalQueryURI to set
	 */
	public void setOriginalQueryURI(String originalQueryURI) {
		this.originalQueryURI = originalQueryURI;
	}

	public void clear() {
		this.hits=null;
		this.pagination=null;
		this.curPage = 1;
		this.lastPage = 1;
		this.farFromStartPage = false;
		this.farFromEndPage = false;
		this.veryFarFromStartPage = false;
		this.veryFarFromEndPage = false;
		this.displayPagination = false;
		this.curPages=null;
	}
}
