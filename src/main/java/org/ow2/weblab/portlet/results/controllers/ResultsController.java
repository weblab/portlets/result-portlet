/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.controllers;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.Constants;
import org.ow2.weblab.portlet.results.bean.MetaConfBean;
import org.ow2.weblab.portlet.results.bean.NavigationBean;
import org.ow2.weblab.portlet.results.services.ResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Results portlet main controller
 *
 * @author lmartin
 *
 */

@Controller(value = "resultsController")
@RequestMapping(value = "VIEW")
@SessionAttributes({ Constants.NAVIGATION_BEAN })
public class ResultsController {

	@Autowired
	@Qualifier("resultsService")
	private ResultsService resultsService;

	@Autowired
	@Qualifier("meta_conf_bean")
	private MetaConfBean metaConfBean;

	// logger
	final static public Log LOGGER = LogFactory.getLog("ResultsPortlet");

	@ModelAttribute(Constants.NAVIGATION_BEAN)
	public NavigationBean initNavigationBean(PortletRequest request, PortletResponse response) {
		return this.resultsService.createNavigationBean();
	}

	@RenderMapping
	public ModelAndView showView(PortletRequest request, PortletResponse response) {
		ModelAndView mav = new ModelAndView(Constants.MAIN_VIEW);
		mav.addObject(Constants.META_CONF_BEAN, this.metaConfBean);
		return mav;
	}

	// event coming from another portlet
	@EventMapping(value = Constants.REACTION_DISPLAY_RESULTS)
	public void displayResults(EventRequest request, EventResponse response, @ModelAttribute(value = Constants.NAVIGATION_BEAN) NavigationBean navigationBean) {
		Object value = request.getEvent().getValue();
		LOGGER.info("New event received " + Constants.REACTION_DISPLAY_RESULTS + " " + value);
		if ((value == null) || (!(value instanceof ResultSet))) {
			LOGGER.error("The Event passed has no or incorrect value " + value);
		}

		// update list of results to display
		this.resultsService.updateResultsList((ResultSet) value, navigationBean);
	}

	// event coming from another portlet in response of "next" page action
	@EventMapping(value = Constants.REACTION_UPDATE_RESULTS)
	public void updateResults(EventRequest request, EventResponse response, @ModelAttribute(value = Constants.NAVIGATION_BEAN) NavigationBean navigationBean) {
		Object value = request.getEvent().getValue();
		LOGGER.info("New event received " + Constants.REACTION_UPDATE_RESULTS + " " + value);
		if ((value == null) || (!(value instanceof ResultSet))) {
			LOGGER.error("The Event passed has no or incorrect value " + value);
		}

		// update list of results to display
		this.resultsService.updateResultsList((ResultSet) value, navigationBean);
	}


	@EventMapping(value = Constants.REACTION_RESET_RESULTS)
	public void resetSearch(EventRequest request, EventResponse response, ModelMap model, @ModelAttribute(value = Constants.NAVIGATION_BEAN) NavigationBean navigationBean, SessionStatus sessionStatus) {
		LOGGER.info("New event received " + Constants.REACTION_RESET_RESULTS);
		this.resultsService.resetResultsList(navigationBean);
		model.clear();
		sessionStatus.setComplete();
	}

	/**
	 * Method use to handle action request asking for a document display
	 *
	 * @param request {@link ActionRequest} current request
	 * @param response {@link ActionResponse} corresponding response
	 * @param navigationBean {@link NavigationBean} current session bean containing current results list
	 * @param documentURI {@link String} URI of document to load
	 */
	@ActionMapping(Constants.DOCUMENT_SELECTION_ACTION)
	public void viewDocument(ActionRequest request, ActionResponse response, @ModelAttribute(value = Constants.NAVIGATION_BEAN) NavigationBean navigationBean,
			@RequestParam(value = Constants.DOCUMENT_URI_PARAM) String documentURI) {
		final PieceOfKnowledge pok = this.resultsService.getDocumentToSendPok(documentURI, navigationBean);
		if (pok != null) {
			response.setEvent(QName.valueOf(Constants.ACTION_SELECT_NOT_LOADED_DOCUMENT), pok);
		}
	}

	/**
	 * Method use to handle action request asking for a page change
	 *
	 * @param request {@link ActionRequest} current request
	 * @param response {@link ActionResponse} corresponding response
	 * @param navigationBean {@link NavigationBean} current session bean containing current results list
	 * @param pageIdx int new page index (starting from 1)

	 */
	@ActionMapping(Constants.UPDATE_PAGE_ACTION)
	public void updatePage(ActionRequest request, ActionResponse response, @ModelAttribute(value = Constants.NAVIGATION_BEAN) NavigationBean navigationBean,
			@RequestParam(value = Constants.PAGE_PARAM) int pageIdx) {

		final PieceOfKnowledge pok = this.resultsService.getNextPagePok(pageIdx, navigationBean);
		if (pok != null) {
			response.setEvent(QName.valueOf(Constants.ACTION_NEXT_RESULTS), pok);
		}
		LOGGER.info("New page to display " + pageIdx + " send event " + Constants.ACTION_NEXT_RESULTS);
	}

	/**
	 * Handle all exceptions that could occur during process. Add an error message that will be displayed in main view
	 *
	 * @param e {@link Exception} Occurring exception
	 * @param request {@link PortletRequest} incoming request
	 * @param response {@link PortletResponse} corresponding response
	 * @return {@link ModelAndView} model and view used to display answer to user
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception e, PortletRequest request, PortletResponse response) {
		LOGGER.error(e.getMessage(), e);

		e.printStackTrace();

		String errorMsg = getLocFmtMsg(request.getLocale(), "errorMessage", e.getMessage());
		LOGGER.info("Display following error message : " + errorMsg);
		ModelAndView mav = new ModelAndView(Constants.MAIN_VIEW);
		mav.addObject(Constants.ERROR_PARAM, errorMsg);
		return mav;
	}

	/**
	 * Try to load a bundle from locale and get the String associated to the key. If args is not empty then format the message before returning it. In case of any MissingResourceException the key is
	 * returned.
	 *
	 * @param locale The locale to be used when searching for a ResourceBundle. Might be null.
	 * @param key The key in the resource file.
	 * @param args The optional arguments to be used when formating the message.
	 * @return The message in the appropriated locale (or the fallback one).
	 */
	protected String getLocFmtMsg(final Locale locale, final String key, Object... args) {
		final ResourceBundle bundle;
		LOGGER.debug("Try to get bundle from Locale " + locale);
		try {
			if (locale == null) {
				bundle = ResourceBundle.getBundle(Constants.BUNDLE);
			} else {
				bundle = ResourceBundle.getBundle(Constants.BUNDLE, locale);
				LOGGER.debug("Loaded bundle is for locale " + bundle.getLocale());
			}
		} catch (MissingResourceException mre) {
			LOGGER.warn("No bundle found for locale " + locale + " and bundle name " + Constants.BUNDLE + ".");
			return key;
		}

		final String message;
		try {
			message = bundle.getString(key);
		} catch (MissingResourceException mre) {
			LOGGER.warn("No message found for locale " + bundle.getLocale() + " and key " + key + ".");
			return key;
		}
		if (args == null || args.length == 0) {
			return message;
		}
		try {
			return MessageFormat.format(message, args);
		} catch (final Exception e) {
			LOGGER.warn("Unable to format message '" + message + "'. Args are: '" + args + "'.");
			return key;
		}
	}

	/**
	 * @return the resultsService
	 */
	public ResultsService getResultsService() {
		return this.resultsService;
	}

	/**
	 * @param resultsService the resultsService to set
	 */
	public void setResultsService(ResultsService resultsService) {
		this.resultsService = resultsService;
	}

	/**
	 * @return the metaConfBean
	 */
	public MetaConfBean getMetaConfBean() {
		return this.metaConfBean;
	}

	/**
	 * @param metaConfBean the metaConfBean to set
	 */
	public void setMetaConfBean(MetaConfBean metaConfBean) {
		this.metaConfBean = metaConfBean;
	}
}
