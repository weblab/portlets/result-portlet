/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.bean;

import java.util.Map;
import java.util.TreeMap;

/**
 * Class representing a result configuration for result portlet.
 *
 * @author emilien
 *
 */
public class MetaConfBean {

	private Map<String, Boolean> properties;

	public MetaConfBean() {
		this.properties = new TreeMap<>();
	}

	public Map<String, Boolean> getProperties() {
		return this.properties;
	}

	public void setProperties(final Map<String, Boolean> properties) {
		this.properties = properties;
	}
}
