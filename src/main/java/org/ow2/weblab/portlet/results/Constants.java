/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.results;

import java.util.HashMap;

public class Constants {

	//view names
	final static public String MAIN_VIEW="mainView";
	final static public String DISPLAY_RESULTS_VIEW="result_portlet_view_resourceCollection";

	//name of session attribute containing user current navigation bean (last list of results, display read results preference...)
	final static public String NAVIGATION_BEAN="navigationBean";
	//meta configuration bean
	final static public String META_CONF_BEAN="meta_conf_bean";

	//name of model attribute containing results list
	final static public String RESULTS_LIST="results";

	// name of HTTP parameter used to identify kind of action
	final static public String ACTION_PARAM = "action";
	// name of HTTP parameter used to identify page to display (when user click on a page number)
	final static public String PAGE_PARAM = "page";
	// name of HTTP parameter used to identify page to display (when user click on a page number)
	final static public String DOCUMENT_URI_PARAM = "docURI";
	// parameter used to identify action was a click on order arrows
	final static public String ORDER_ACTION = "orderAction";
	//HTTP param used to indicate which is result is newly set as current
	final static public String CURRENT_RESULT_PARAM = "newResultURI";
	//HTTP param used to trasfer "show read results flag"
	final static public String READ_RESULTS_FLAG_PARAM = "showReadResultsFlag";

	//specific HTTP parameter
	final static public String RANK_PROPERTY_PARAM="rankProperty";
	final static public String DATE_PROPERTY="hasGatheringDate";
	final static public String PRIORITY_PROPERTY="prio-filtre";

	//resources URL
	final static public String DISPLAY_RESULTS_RESOURCE="displayResultsURL";

	//action URL
	final static public String UPDATE_PAGE_ACTION="updatePageAction";
	// parameter used to identify action was a click on a resource in results list
	final static public String DOCUMENT_SELECTION_ACTION = "documentSelection";

	//Events
	final static public String REACTION_DISPLAY_RESULTS="{http://weblab.ow2.org/portlet/reaction}displayResults";
	final static public String REACTION_UPDATE_RESULTS="{http://weblab.ow2.org/portlet/reaction}updateResults";
	final static public String REACTION_RESET_RESULTS="{http://weblab.ow2.org/portlet/reaction}resetResults";
	final static public String ACTION_NEXT_RESULTS="{http://weblab.ow2.org/portlet/action}nextDocumentsSelection";
	final static public String ACTION_SELECT_NOT_LOADED_DOCUMENT="{http://weblab.ow2.org/portlet/action}notLoadedDocumentSelection";

	final static public String ERROR_PARAM="errorMsg";

	//name of resource bundle used in this portlet
	final static public String BUNDLE="result_portlet";

	//URIs used while building POK to send via event to other portlets
	final static public String DOC_VIEW_HIT_URI_PREFIX="weblab://resultsToDocView";
	final static public String PORTLET_URI="weblab://resultsPortlet";

	static public HashMap<String, String>ORDER_PROPERTIES_URI=new HashMap<>();
	static {
		ORDER_PROPERTIES_URI.put(DATE_PROPERTY, "http://weblab.ow2.org/core/1.2/ontology/processing#hasGatheringDate");
	}
}
