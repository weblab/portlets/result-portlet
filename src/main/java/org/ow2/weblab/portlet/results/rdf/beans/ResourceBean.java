/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.rdf.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.ow2.weblab.core.extended.ontologies.DCTerms;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.extended.ontologies.RDFS;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.rdf.RDFObject;
import org.ow2.weblab.portlet.results.rdf.RDFPropertyAnnotation;

/**
 * Bean use to store all data we could meet in a WebLab RDF Resource. ResultSet built by search service contains a list of Hit. Each Hit refers to a Resource
 *
 * @see Resource
 * @see ResultSet
 * @see ResultSetBean
 * @see HitBean
 * @author lmartin
 *
 */
public class ResourceBean extends RDFObject{

	//Documents properties
	@RDFPropertyAnnotation(property = WebLabProcessing.HAS_NATIVE_CONTENT)
	private String hasNativeContent;
	@RDFPropertyAnnotation(property = WebLabProcessing.HAS_NORMALISED_CONTENT)
	private String hasNormalisedContent;
	@RDFPropertyAnnotation(property = WebLabProcessing.IS_GENERATED_FROM)
	private String isGeneratedFrom;
	@RDFPropertyAnnotation(property = WebLabProcessing.IS_PRODUCED_BY)
	private String isProducedBy;
	@RDFPropertyAnnotation(property = WebLabProcessing.NAMESPACE+"isProducedFrom")
	private String isProducedFrom;

	@RDFPropertyAnnotation(property = WebLabProcessing.IS_EXPOSED_AS)
	private String isExposedAs;
	@RDFPropertyAnnotation(property = WebLabProcessing.IS_EXPOSED_AS_THUMBNAIL)
	private String isExposedAsThumbnail;

	@RDFPropertyAnnotation(property = WebLabProcessing.HAS_GATHERING_DATE)
	private Date gatheringDate;
	@RDFPropertyAnnotation(property="http://weblab-project.org/core/model/property/crawler/projectName")
	private String projectName;
	@RDFPropertyAnnotation(property = WebLabProcessing.HAS_ORIGINAL_FILE_SIZE)
	private long hasOriginalFileSize;
	@RDFPropertyAnnotation(property = WebLabProcessing.HAS_ORIGINAL_FILE_NAME)
	private String hasOriginalFileName;
	@RDFPropertyAnnotation(property = WebLabProcessing.REFERS_TO)
	private String refersTo;

	//DublinCore properties
	@RDFPropertyAnnotation(property = DublinCore.TITLE_PROPERTY_NAME)
	private String title;
	@RDFPropertyAnnotation(property = DublinCore.CREATOR_PROPERTY_NAME)
	private String creator;
	@RDFPropertyAnnotation(property = DublinCore.SUBJECT_PROPERTY_NAME)
	private String subject;
	@RDFPropertyAnnotation(property = DCTerms.SUBJECT)
	private String catego;
	@RDFPropertyAnnotation(property = DublinCore.DESCRIPTION_PROPERTY_NAME)
	private String description;
	@RDFPropertyAnnotation(property = DublinCore.PUBLISHER_PROPERTY_NAME)
	private String publisher;
	@RDFPropertyAnnotation(property = DublinCore.CONTRIBUTOR_PROPERTY_NAME)
	private String contibutor;
	@RDFPropertyAnnotation(property = DublinCore.DATE_PROPERTY_NAME)
	private Date date;
	@RDFPropertyAnnotation(property = DublinCore.TYPE_PROPERTY_NAME)
	private String type;
	@RDFPropertyAnnotation(property = DublinCore.FORMAT_PROPERTY_NAME)
	private String format;
	@RDFPropertyAnnotation(property = DublinCore.IDENTIFIER_PROPERTY_NAME)
	private String identifier;
	@RDFPropertyAnnotation(property = DublinCore.SOURCE_PROPERTY_NAME)
	private String source;
	@RDFPropertyAnnotation(property = DublinCore.LANGUAGE_PROPERTY_NAME)
	private String language;
	@RDFPropertyAnnotation(property = DublinCore.RELATION_PROPERTY_NAME)
	private String relation;
	@RDFPropertyAnnotation(property = DublinCore.COVERAGE_PROPERTY_NAME)
	private String coverage;
	@RDFPropertyAnnotation(property = DublinCore.RIGHTS_PROPERTY_NAME)
	private String right;
	@RDFPropertyAnnotation(property = DCTerms.MODIFIED)
	private Date modified;
	@RDFPropertyAnnotation(property = DCTerms.CREATED)
	private Date created;
	@RDFPropertyAnnotation(property = DCTerms.EXTENT)
	private String extent;
	@RDFPropertyAnnotation(property = DCTerms.ABSTRACT)
	private String isAbstract;

	//FOAF properties
	@RDFPropertyAnnotation(property = "http://xmlns.com/foaf/0.1/thumbnail")
	private String thumbnail;
	@RDFPropertyAnnotation(property = "http://xmlns.com/foaf/0.1/firstName")
	private String firstName;
	@RDFPropertyAnnotation(property = "http://xmlns.com/foaf/0.1/lastName")
	private String lastName;

	//RDF properties
	@RDFPropertyAnnotation(property = RDFS.SEE_ALSO)
	private String seeAlso;

	@RDFPropertyAnnotation(property = RDF.TYPE)
	private String rdfType;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HIT)
	private String hitType;

	// Search properties
	@RDFPropertyAnnotation(property = WebLabRetrieval.NAMESPACE+"hasThumbnailSnippet")
	private List<String> hasThumbnailSnippet;

	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_RELEVANT_MEDIA_UNIT)
	private String hasRelevantMediaUnit;
	//" value="http://weblab.ow2.org/core/1.2/ontology/retrieval#isLinkedToRelevantMediaUnit" />
	@RDFPropertyAnnotation(property = WebLabRetrieval.IN_RESULT_SET_HIT)
	private String inResultSetHit;
	//value="http://weblab.ow2.org/core/1.2/ontology/retrieval#inResultSet" />
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_EXPECTED_OFFSET)
	private int queryOffset;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_EXPECTED_LIMIT)
	private int queryLimit;


	// DateFormat used for PJ
	final static public SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return this.language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return this.format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the gatheringDate
	 */
	public Date getGatheringDate() {
		return this.gatheringDate;
	}

	/**
	 * @param gatheringDate the gatheringDate to set
	 */
	public void setGatheringDate(Date gatheringDate) {
		this.gatheringDate = gatheringDate;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the thumbnail
	 */
	public String getThumbnail() {
		return this.thumbnail;
	}

	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the queryOffset
	 */
	public int getQueryOffset() {
		return this.queryOffset;
	}

	/**
	 * @param queryOffset the queryOffset to set
	 */
	public void setQueryOffset(int queryOffset) {
		this.queryOffset = queryOffset;
	}

	/**
	 * @return the queryLimit
	 */
	public int getQueryLimit() {
		return this.queryLimit;
	}

	/**
	 * @param queryLimit the queryLimit to set
	 */
	public void setQueryLimit(int queryLimit) {
		this.queryLimit = queryLimit;
	}

	/**
	 * @return the hasNativeContent
	 */
	public String getHasNativeContent() {
		return this.hasNativeContent;
	}

	/**
	 * @param hasNativeContent the hasNativeContent to set
	 */
	public void setHasNativeContent(String hasNativeContent) {
		this.hasNativeContent = hasNativeContent;
	}

	/**
	 * @return the hasNormalisedContent
	 */
	public String getHasNormalisedContent() {
		return this.hasNormalisedContent;
	}

	/**
	 * @param hasNormalisedContent the hasNormalisedContent to set
	 */
	public void setHasNormalisedContent(String hasNormalisedContent) {
		this.hasNormalisedContent = hasNormalisedContent;
	}

	/**
	 * @return the isGeneratedFrom
	 */
	public String getIsGeneratedFrom() {
		return this.isGeneratedFrom;
	}

	/**
	 * @param isGeneratedFrom the isGeneratedFrom to set
	 */
	public void setIsGeneratedFrom(String isGeneratedFrom) {
		this.isGeneratedFrom = isGeneratedFrom;
	}

	/**
	 * @return the isProducedBy
	 */
	public String getIsProducedBy() {
		return this.isProducedBy;
	}

	/**
	 * @param isProducedBy the isProducedBy to set
	 */
	public void setIsProducedBy(String isProducedBy) {
		this.isProducedBy = isProducedBy;
	}

	/**
	 * @return the isExposedAs
	 */
	public String getIsExposedAs() {
		return this.isExposedAs;
	}

	/**
	 * @param isExposedAs the isExposedAs to set
	 */
	public void setIsExposedAs(String isExposedAs) {
		this.isExposedAs = isExposedAs;
	}

	/**
	 * @return the isExposedAsThumbnail
	 */
	public String getIsExposedAsThumbnail() {
		return this.isExposedAsThumbnail;
	}

	/**
	 * @param isExposedAsThumbnail the isExposedAsThumbnail to set
	 */
	public void setIsExposedAsThumbnail(String isExposedAsThumbnail) {
		this.isExposedAsThumbnail = isExposedAsThumbnail;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the hasOriginalFileSize
	 */
	public long getHasOriginalFileSize() {
		return this.hasOriginalFileSize;
	}

	/**
	 * @param hasOriginalFileSize the hasOriginalFileSize to set
	 */
	public void setHasOriginalFileSize(long hasOriginalFileSize) {
		this.hasOriginalFileSize = hasOriginalFileSize;
	}

	/**
	 * @return the hasOriginalFileName
	 */
	public String getHasOriginalFileName() {
		return this.hasOriginalFileName;
	}

	/**
	 * @param hasOriginalFileName the hasOriginalFileName to set
	 */
	public void setHasOriginalFileName(String hasOriginalFileName) {
		this.hasOriginalFileName = hasOriginalFileName;
	}

	/**
	 * @return the refersTo
	 */
	public String getRefersTo() {
		return this.refersTo;
	}

	/**
	 * @param refersTo the refersTo to set
	 */
	public void setRefersTo(String refersTo) {
		this.refersTo = refersTo;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return this.creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the catego
	 */
	public String getCatego() {
		return this.catego;
	}

	/**
	 * @param catego the catego to set
	 */
	public void setCatego(String catego) {
		this.catego = catego;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return this.publisher;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 * @return the contibutor
	 */
	public String getContibutor() {
		return this.contibutor;
	}

	/**
	 * @param contibutor the contibutor to set
	 */
	public void setContibutor(String contibutor) {
		this.contibutor = contibutor;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return this.identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return this.source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the relation
	 */
	public String getRelation() {
		return this.relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return this.coverage;
	}

	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * @return the right
	 */
	public String getRight() {
		return this.right;
	}

	/**
	 * @param right the right to set
	 */
	public void setRight(String right) {
		this.right = right;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() {
		return this.modified;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) {
		this.modified = modified;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return this.created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the extent
	 */
	public String getExtent() {
		return this.extent;
	}

	/**
	 * @param extent the extent to set
	 */
	public void setExtent(String extent) {
		this.extent = extent;
	}

	/**
	 * @return the isAbstract
	 */
	public String getIsAbstract() {
		return this.isAbstract;
	}

	/**
	 * @param isAbstract the isAbstract to set
	 */
	public void setIsAbstract(String isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the seeAlso
	 */
	public String getSeeAlso() {
		return this.seeAlso;
	}

	/**
	 * @param seeAlso the seeAlso to set
	 */
	public void setSeeAlso(String seeAlso) {
		this.seeAlso = seeAlso;
	}

	/**
	 * @return the rdfType
	 */
	public String getRdfType() {
		return this.rdfType;
	}

	/**
	 * @param rdfType the rdfType to set
	 */
	public void setRdfType(String rdfType) {
		this.rdfType = rdfType;
	}

	/**
	 * @return the hitType
	 */
	public String getHitType() {
		return this.hitType;
	}

	/**
	 * @param hitType the hitType to set
	 */
	public void setHitType(String hitType) {
		this.hitType = hitType;
	}

	/**
	 * @return the hasThumbnailSnippet
	 */
	public List<String> getHasThumbnailSnippet() {
		return this.hasThumbnailSnippet;
	}

	/**
	 * @param hasThumbnailSnippet the hasThumbnailSnippet to set
	 */
	public void setHasThumbnailSnippet(List<String> hasThumbnailSnippet) {
		this.hasThumbnailSnippet = hasThumbnailSnippet;
	}

	/**
	 * @return the hasRelevantMediaUnit
	 */
	public String getHasRelevantMediaUnit() {
		return this.hasRelevantMediaUnit;
	}

	/**
	 * @param hasRelevantMediaUnit the hasRelevantMediaUnit to set
	 */
	public void setHasRelevantMediaUnit(String hasRelevantMediaUnit) {
		this.hasRelevantMediaUnit = hasRelevantMediaUnit;
	}

	/**
	 * @return the inResultSetHit
	 */
	public String getInResultSetHit() {
		return this.inResultSetHit;
	}

	/**
	 * @param inResultSetHit the inResultSetHit to set
	 */
	public void setInResultSetHit(String inResultSetHit) {
		this.inResultSetHit = inResultSetHit;
	}

	/**
	 * @return the isProducedFrom
	 */
	public String getIsProducedFrom() {
		return this.isProducedFrom;
	}

	/**
	 * @param isProducedFrom the isProducedFrom to set
	 */
	public void setIsProducedFrom(String isProducedFrom) {
		this.isProducedFrom = isProducedFrom;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
