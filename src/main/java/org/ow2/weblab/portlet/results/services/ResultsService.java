/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.bean.NavigationBean;

/**
 * Specific service used in MVC Results portlet. Use to init sessions bean that will be used during user's navigation, update list of currently displayed results, reset this list
 * @author lmartin
 */
public abstract class ResultsService {

	// logger used by all meta view portlet classes
	final static public Log LOGGER = LogFactory.getLog("resultsPortlet");

	/**
	 * Create a new bean that will be used during user's navigation (stored in session)
	 * @return {@link NavigationBean} new ResultBean for this user
	 */
	public NavigationBean createNavigationBean() {
		NavigationBean navigationBean=new NavigationBean();
		//in pages display : 2 pages before current & 2 pages after current are displayed
		navigationBean.setPagesRadius(2);
		return navigationBean;
	}

	/**
	 * Update list of results to display from a {@link ResultSet}
	 * @param resultSet {@link ResultSet} results set received from search portlet and corresponding to new list of results to display
	 * @param navigationBean {@link NavigationBean} current navigation configuration
	 */
	public abstract void updateResultsList(ResultSet resultSet,NavigationBean navigationBean);

	/**
	 * Reset list of results to display
	 * @param navigationBean {@link NavigationBean} current navigation configuration
	 */
	public abstract void resetResultsList(NavigationBean navigationBean);

	public abstract PieceOfKnowledge getDocumentToSendPok(String docURI,NavigationBean navigationBean);

	public abstract PieceOfKnowledge getNextPagePok(int pageIdx,NavigationBean navigationBean);
}
