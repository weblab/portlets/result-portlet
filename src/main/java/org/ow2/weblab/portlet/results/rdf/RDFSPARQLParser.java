/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.rdf;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.portlet.results.rdf.beans.ResourceBean;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Class providing methods to parse a RDF {@link PieceOfKnowledge} using a SPARQL
 * query and building a corresponding bean Class of object to return is
 * specified when calling parse methods and must use {@link RDFObjectAnnotation}
 * and {@link RDFPropertyAnnotation} annotations to specifiy which objects/
 * fields correspond to which RDF property that could be met in RDF source
 *
 * @author lmartin, jdoucy
 *
 * @see RDFObjectAnnotation
 * @see RDFPropertyAnnotation
 * @see RDFObject
 *
 */
public class RDFSPARQLParser {

	// default SPARQL query use during RDF parsing
	final static public String DEFAULT_SPARQL_QUERY = "select distinct ?o ?p ?v where {?o ?p ?v} order by ?o";

	// default value for object / property / value used in SPARQL query
	final static public String OBJECT_VAR_NAME = "o";
	final static public String PROPERTY_VAR_NAME = "p";
	final static public String VALUE_VAR_NAME = "v";

	// logger used by all meta view portlet classes
	final static public Log LOGGER = LogFactory.getLog(RDFSPARQLParser.class);

	/**
	 * Parse a RDF content and fill an ouput resource with values found in RDF,
	 * using default SPARQLQuery. Values are completed using
	 * {@link RDFPropertyAnnotation}
	 * @param inputPok
	 *            {@link PieceOfKnowledge} input to parse (RDF format)
	 * @param outputClass
	 *            {@link Object} bean to complete
	 * @param <T> The type of object to be found
	 * @return an instance of outputClass
	 */
	static public <T extends RDFObject> T parse(PieceOfKnowledge inputPok, Class<T> outputClass) {
		return parse(inputPok, outputClass, DEFAULT_SPARQL_QUERY, OBJECT_VAR_NAME, PROPERTY_VAR_NAME, VALUE_VAR_NAME);
	}

	/**
	 * Parse a RDF content and fill an ouput resource with values found in RDF.
	 * Values are completed using {@link RDFPropertyAnnotation}
	 *
	 * @param inputPok
	 *            {@link PieceOfKnowledge} input to parse (RDF format)
	 * @param outputClass
	 *            {@link Object} bean to complete
	 * @param SPARQLQuery
	 *            {@link String} query use to parse input RDF
	 * @param objectVarName
	 *            {@link String} var name use for objet in SPARQL query (?o)
	 * @param propertyVarName
	 *            {@link String} var name use for property in SPARQL query (?p)
	 * @param valueVarName
	 *            {@link String} name use for valueVarName in SPARQL query (?v)
	 * @param <T> The type of object to be found
	 * @return A instance of outputClass
	 */
	static public <T extends RDFObject> T parse(PieceOfKnowledge inputPok, final Class<T> outputClass, String SPARQLQuery, String objectVarName,
			String propertyVarName, String valueVarName) {
		if (inputPok == null) {
			LOGGER.warn("Input resource is null -> nothing to parse");
			return null;
		}
		RDFObjectAnnotation objectAnnotation = outputClass.getAnnotation(RDFObjectAnnotation.class);
		if (objectAnnotation == null) {
			LOGGER.warn("This class do not have any RDFObjectAnnotation: " + outputClass.getName());
			return null;
		}

		// new bean we will complete and return
		T result = null;

		// list of property / value pair for each object found in sparql query
		// results (HashMap<o, {p,v}>)
		HashMap<String, List<RDFProperty>> properties = new HashMap<>();

		// Get list of Hits of this ResultSet
		SemanticResource semRes = new SemanticResource(inputPok);
		try {
			com.hp.hpl.jena.query.ResultSet jenaResultSet = semRes.selectAsJenaResultSet(SPARQLQuery);
			while (jenaResultSet.hasNext()) {
				QuerySolution querySolution = jenaResultSet.next();

				// get object / property / value
				// TODO check resource casting
				Resource objectResource = querySolution.getResource(objectVarName);
				Resource propertyResource = querySolution.getResource(propertyVarName);
				RDFNode valueNode = querySolution.get(valueVarName);

				// do we already have some properties for this object
				List<RDFProperty> curProps = properties.get(objectResource.getURI());
				if (curProps == null) {
					curProps = new ArrayList<>();
					properties.put(objectResource.getURI(), curProps);
				}
				/*
				 * add property value to the map in order to populate beans
				 */
				RDFProperty prop = new RDFProperty(propertyResource.toString(), valueNode.toString());
				if (!curProps.contains(prop)) {
					curProps.add(prop);
				}
			}
			// 1st iteration : look for object to build
			for (String objURI : properties.keySet()) {
				List<RDFProperty> props = properties.get(objURI);
				for (RDFProperty prop : props) {
					if (objectAnnotation.property().equals(prop.value)) {
						result = outputClass.newInstance();
						result.setURI(objURI);
						break;
					}
				}
				if (result != null) {
					break;
				}
			}
			// could not instanciate result -> end
			if (result == null) {
				LOGGER.warn("Could not create a new instance: " + outputClass.getName() + " no matching annotation: " + objectAnnotation.property());
				return null;
			}

			// last step : complete all beans with RDF properties
			ArrayList<RDFObject> beansToComplete = new ArrayList<>();
			beansToComplete.add(result);
			// iterate on beansToComplete list until it is empty
			while (!beansToComplete.isEmpty()) {
				ArrayList<RDFObject> tmpBeans = new ArrayList<>();
				tmpBeans.addAll(beansToComplete);
				for (RDFObject bean : tmpBeans) {
					List<RDFProperty> props = properties.get(bean.getURI());
					for (RDFProperty prop : props) {
						// complete current bean with this new property
						RDFObject subBean = completeBean(bean, prop.name, prop.value.replaceAll("\\^\\^http://www.w3.org/2001/XMLSchema#.+", ""));
						// this completion create a new subbean -> we will add
						// it to beansToComplete list and will try to complete
						// it at next iteration
						if (subBean != null) {
							beansToComplete.add(subBean);
						}
					}
					beansToComplete.remove(bean);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * Fill in a Hit field from a RDF property name and value
	 * @param bean
	 *           The bean to fill in
	 * @param propertyName
	 *            {@link String} name of current RDF property
	 * @param propertyValue
	 *            {@link String} corresponding value
	 * @return The object
	 * @throws IllegalAccessException If something wrong occurs
	 * @throws InstantiationException If something wrong occurs
	 * @throws IllegalArgumentException If something wrong occurs
	 * @throws ParseException If something wrong occurs
	 */
	static public RDFObject completeBean(Object bean, String propertyName, String propertyValue) throws IllegalAccessException, InstantiationException,
			IllegalArgumentException, ParseException {
		if (bean == null) {
			LOGGER.error("Could not fill in bean: it is null !!");
			return null;
		}

		for (Field field : getAllFields(bean.getClass())) {
			Annotation annotation = field.getAnnotation(RDFPropertyAnnotation.class);
			// first case : "simple" field (Long, String, Date...) identified
			// with a RDFProperty annotation
			if (annotation != null) {
				for (String annot : ((RDFPropertyAnnotation) annotation).property()) {
					if (annot.equals(propertyName)) {
						field.setAccessible(true);
						Class<?> type = field.getType();
						if ((boolean.class.isAssignableFrom(type))) {
							field.set(bean, Boolean.valueOf(propertyValue));
						} else if ((int.class.isAssignableFrom(type))) {
							field.set(bean, Integer.valueOf(propertyValue));
						} else if ((long.class.isAssignableFrom(type))) {
							field.set(bean, Long.valueOf(propertyValue));
						} else if ((float.class.isAssignableFrom(type))) {
							field.set(bean, Float.valueOf(propertyValue));
						} else if ((double.class.isAssignableFrom(type))) {
							field.set(bean, Double.valueOf(propertyValue));
						} else if ((List.class.isAssignableFrom(type))) {
							List curList = (List) field.get(bean);
							if (curList == null) {
								curList = (List) type.newInstance();
								field.set(bean, curList);
							}
							curList.add(propertyValue);
						} else if (String.class.isAssignableFrom(type)) {
							field.set(bean, propertyValue);
						} else if (Date.class.isAssignableFrom(type)) {
							field.set(bean, ResourceBean.DATE_FORMAT.parse(propertyValue));
							// other case : we have a sub bean to fill
						} else {
							RDFObject subBean = (RDFObject) type.newInstance();
							subBean.setURI(propertyValue);
						}
						return null;
					}
				}
			}

			// 2nd case : do we have a sub bean to fill (identified with
			// RDFObject annotation)
			annotation = field.getAnnotation(RDFObjectAnnotation.class);
			if (annotation != null) {
				if (((RDFObjectAnnotation) annotation).property().equals(propertyName)) {
					field.setAccessible(true);
					Class type = field.getType();
					// list case: main bean contains a list of subbeans with
					// this specific annotation
					if ((List.class.isAssignableFrom(type))) {
						List<RDFObject> curList = (List<RDFObject>) field.get(bean);
						if (curList == null) {
							curList = (List<RDFObject>) type.newInstance();
							field.set(bean, curList);
						}
						RDFObject subBean = (RDFObject) ((RDFObjectAnnotation) annotation).objectClass().newInstance();
						subBean.setURI(propertyValue);
						curList.add(subBean);
						return subBean;
						// other case : we have a "unique" sub bean to complete
					}
					RDFObject subBean = (RDFObject) ((RDFObjectAnnotation) annotation).objectClass().newInstance();
					subBean.setURI(propertyValue);
					field.set(bean, subBean);
					return subBean;
				}
			}
		}
		LOGGER.debug("No field found fot this property: " + propertyName + " value: " + propertyValue);
		return null;
	}

	/**
	 * List all fields of a specific class, including inherited field
	 *
	 * @param type
	 *            {@link Class} class for which we want to list fields
	 * @return {@link List} corresponding list of fields
	 */
	public static List<Field> getAllFields(Class<?> type) {
		List<Field> fields = new ArrayList<>();
		for (Class<?> c = type; c != null; c = c.getSuperclass()) {
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
		}
		return fields;
	}

	// inner class use to store property retrieved during RDF parsing : (name +
	// value)
	static class RDFProperty {
		protected String name;
		protected String value;

		RDFProperty(String name, String value) {
			this.name = name;
			this.value = value;
		}


		@Override
		public String toString() {
			return "RDFProperty: " + this.name + "=" + this.value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
			result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RDFProperty other = (RDFProperty) obj;
			if (this.name == null) {
				if (other.name != null)
					return false;
			} else if (!this.name.equals(other.name))
				return false;
			if (this.value == null) {
				if (other.value != null)
					return false;
			} else if (!this.value.equals(other.value))
				return false;
			return true;
		}


	}
}
