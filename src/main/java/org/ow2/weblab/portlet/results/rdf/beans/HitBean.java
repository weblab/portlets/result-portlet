/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.rdf.beans;

import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.rdf.RDFObject;
import org.ow2.weblab.portlet.results.rdf.RDFObjectAnnotation;
import org.ow2.weblab.portlet.results.rdf.RDFPropertyAnnotation;

/**
 * Bean use to store all data we could meet in an RDF ResultSet "Hit". ResultSet built by search service contains a list of Hits. Each Hit refers to a Resource
 *
 * @see ResultSet
 * @see ResultSetBean
 * @author lmartin
 *
 */
public class HitBean extends RDFObject {

	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_SCORE)
	private float hasScore;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_RANK)
	private int rank;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_DESCRIPTION)
	private String hasDescription;
	@RDFObjectAnnotation(property = WebLabRetrieval.IS_LINKED_TO, objectClass = ResourceBean.class)
	private ResourceBean resource;

	/**
	 * @return the rank
	 */
	public int getRank() {
		return this.rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * @return the hasDescription
	 */
	public String getHasDescription() {
		return this.hasDescription;
	}

	/**
	 * @param hasDescription the hasDescription to set
	 */
	public void setHasDescription(String hasDescription) {
		this.hasDescription = hasDescription;
	}

	/**
	 * @return the resource
	 */
	public ResourceBean getResource() {
		return this.resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(ResourceBean resource) {
		this.resource = resource;
	}

	/**
	 * @return the hasScore
	 */
	public float getHasScore() {
		return this.hasScore;
	}

	/**
	 * @param hasScore the hasScore to set
	 */
	public void setHasScore(float hasScore) {
		this.hasScore = hasScore;
	}

}
