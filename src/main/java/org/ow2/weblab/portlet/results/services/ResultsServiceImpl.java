/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.portlet.results.Constants;
import org.ow2.weblab.portlet.results.Pagination;
import org.ow2.weblab.portlet.results.bean.NavigationBean;
import org.ow2.weblab.portlet.results.rdf.RDFSPARQLParser;
import org.ow2.weblab.portlet.results.rdf.beans.HitBean;
import org.ow2.weblab.portlet.results.rdf.beans.ResourceBean;
import org.ow2.weblab.portlet.results.rdf.beans.ResultSetBean;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * WebLab OW2 ResultsService implementation
 *
 * @author lmartin
 *
 */
public class ResultsServiceImpl extends ResultsService {

	@Override
	public void updateResultsList(ResultSet resultSet, NavigationBean navigationBean) {
		// parse POK of current resultset as a bean and update navigationbean
		// with new values
		ResultSetBean resultSetBean = RDFSPARQLParser.parse(resultSet.getPok(), ResultSetBean.class);
		if (resultSetBean == null) {
			navigationBean.clear();
		} else {

			navigationBean.setPagination(new Pagination(resultSetBean.getHasNumberOfResults(), resultSetBean.getLimit(), resultSetBean.getOffset()));
			// fill a new hits list in current navigation bean
			ArrayList<HitBean> hits = new ArrayList<>();
			if (resultSetBean.getHits() != null) {
				hits.addAll(resultSetBean.getHits());
			}

			// order hits on rank
			Collections.sort(hits, new Comparator<HitBean>() {
				@Override
				public int compare(HitBean arg0, HitBean arg1) {
					return Integer.compare(arg0.getRank(), arg1.getRank());
				}
			});
			navigationBean.setHits(hits);
			navigationBean.setOriginalQueryURI(resultSetBean.getIsResultOf());
		}
	}

	@Override
	public void resetResultsList(NavigationBean navigationBean) {
		navigationBean.clear();
	}

	@Override
	public PieceOfKnowledge getDocumentToSendPok(String docURI, NavigationBean navigationBean) {
		LOGGER.info("Build a new Pok to send to DocView portlet for document " + docURI);
		final PieceOfKnowledge pok = new PieceOfKnowledge();
		pok.setUri(UUID.randomUUID().toString());

		final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(pok.getUri()), pok);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(URI.create(pok.getUri()), pok);

		for (HitBean hit : navigationBean.getHits()) {
			ResourceBean res = hit.getResource();
			String resURI = res.getURI();
			if (resURI.equals(docURI)) {
				// Create a Hit which is linked to a Resource.
				URI newHitURI = URI.create(Constants.DOC_VIEW_HIT_URI_PREFIX + System.nanoTime());
				URI resourceURI = URI.create(docURI);
				wra.startInnerAnnotatorOn(newHitURI);
				wra.writeType(URI.create(WebLabRetrieval.HIT));
				wra.writeLinkedTo(resourceURI);
				wra.endInnerAnnotator();

				// Describe the resource with available information about it
				dca.startInnerAnnotatorOn(resourceURI);
				final String format = res.getFormat();
				if (format != null && !format.isEmpty()) {
					dca.writeFormat(format);
				}
				final String language = res.getLanguage();
				if (language != null && !language.isEmpty()) {
					dca.writeLanguage(language);
				}
				final String title = res.getTitle();
				if (title != null && !title.isEmpty()) {
					dca.writeTitle(title);
				}
				final String type = res.getType();
				if (type != null && !type.isEmpty()) {
					dca.writeType(type);
				}
				final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(pok.getUri()), pok);
				wpa.writeProducedBy(URI.create(Constants.PORTLET_URI));
				return pok;
			}
		}
		LOGGER.error("This resource was not found in current navigation bean: " + docURI);
		return null;
	}

	@Override
	public PieceOfKnowledge getNextPagePok(int pageIdx, NavigationBean navigationBean) {
		Pagination pagination = navigationBean.getPagination();
		pagination.setCurrentPage(pageIdx);

		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("resultporlet", "" + new Date().getTime(), PieceOfKnowledge.class);
		final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(pok.getUri()), pok);
		wra.writeType(URI.create(WebLabRetrieval.SEARCH_ORDER));
		wra.writeOrderedQuery(URI.create(navigationBean.getOriginalQueryURI()));
		wra.writeExpectedOffset(Integer.valueOf(pagination.getStartIndex()));
		wra.writeExpectedLimit(Integer.valueOf(pagination.getItemsPerPage()));
		return pok;
	}
}
