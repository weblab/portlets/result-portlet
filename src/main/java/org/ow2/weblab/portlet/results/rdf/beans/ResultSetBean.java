/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.results.rdf.beans;

import java.util.ArrayList;
import java.util.List;

import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.results.rdf.RDFObject;
import org.ow2.weblab.portlet.results.rdf.RDFObjectAnnotation;
import org.ow2.weblab.portlet.results.rdf.RDFPropertyAnnotation;

/**
 * Bean use to stored properties retrieved in a {@link ResultSet}. ResultSet built by search service contains a list of Hits. Each Hit refers to a Resource
 *
 * @see ResultSet
 * @see ResultSetBean
 * @see HitBean
 * @see ResourceBean
 * @author lmartin
 *
 */
@RDFObjectAnnotation(property = WebLabModel.RESULT_SET, objectClass = ResultSetBean.class)
public class ResultSetBean extends RDFObject {

	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_NUMBER_OF_RESULTS)
	private int hasNumberOfResults;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_EXPECTED_OFFSET)
	private int offset;
	@RDFPropertyAnnotation(property = WebLabRetrieval.HAS_EXPECTED_LIMIT)
	private int limit;
	@RDFPropertyAnnotation(property = WebLabRetrieval.IS_RESULT_OF)
	private String isResultOf;

	@RDFObjectAnnotation(property = WebLabRetrieval.HAS_HIT, objectClass = HitBean.class)
	private ArrayList<HitBean> hits;

	/**
	 * @return the hasNumberOfResults
	 */
	public int getHasNumberOfResults() {
		return this.hasNumberOfResults;
	}

	/**
	 * @param hasNumberOfResults the hasNumberOfResults to set
	 */
	public void setHasNumberOfResults(int hasNumberOfResults) {
		this.hasNumberOfResults = hasNumberOfResults;
	}

	/**
	 * @return the offset
	 */
	public int getOffset() {
		return this.offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return this.limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the hits
	 */
	public List<HitBean> getHits() {
		return this.hits;
	}

	/**
	 * @param hits the hits to set
	 */
	public void setHits(ArrayList<HitBean> hits) {
		this.hits = hits;
	}

	/**
	 * @return the isResultOf
	 */
	public String getIsResultOf() {
		return this.isResultOf;
	}

	/**
	 * @param isResultOf the isResultOf to set
	 */
	public void setIsResultOf(String isResultOf) {
		this.isResultOf = isResultOf;
	}

}
